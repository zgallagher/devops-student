## Assignment 1##

**Due Dates:**

- As mentioned the pre course work is part of your homework 1, please have it done by *09/09 11:55pm Saturday*.
- Homework please have it done by *09/22 11:55pm Friday*.

###PreCourse Work:###

Hand in under Sakai->Assignment->PreCourseWork

**TODO:**  Review the links below that have PC mark on them. Then you should be able to the work.

You created a bitbucket account now what?

1. Create a repo called "devops-student"
   
    - Add me as a user (msaenz(AT)luc.edu)that has admin rights
    - Make sure your repo is private
    - We are using git
    - Make sure to put description (look at picture in examples folder)
    - **Deliverable: I should be able to access this repo, send me the link to clone in Sakai**

2. Git Administration via commandline

    - Clone your repo
    - Copy the README.md in the examples folder into your local repo folder
    - Add the file
    - Commit the change with a message "This is an initial commit"
    - Push changes to your repo.
    - **Deliverable: A screenshot of the commands you ran with their outcomes**

3. Answer the following questions in a file called: *gitqs-<userid>.txt*

    - example name gitqs-msaenz.txt
    - What is the difference b/t forking and branching?
    - How do create a branch and merge it to master (please list out the commands)
    - What are the differences between Mercurial and Git?
    - **Deliverable: the file gitqs-<userid>.txt**


*If you are having trouble please install GIT from the web*

**Please upload the 3 deliverables to Sakai.**

###Homework 1:###

Hand in under Sakai->Assignment->Homework_1


1. Download the AirlineSim Folder

    - Create a UML Diagram of the system
    - **Deliverable: A UML Diagram**

2. Run the Simulator

    - Run the Simulator for the Airline and I would like that output of it running via textfile called **output-<uid>.txt** and a copy of the console in a screenshot.

    - Create a file called: **explain-<uid>.txt** and explain to me in English, the process flow of the AirlineCheckSim.java

   - **Deliverables:**
       - A **screenshot** of AirlineSim run
       - A **output-<uid>.txt** with the console output
       - A **explain-<uid>.txt** of how the Simulator works.

*If you are having trouble please install GIT from the web*

Please upload the 4 deliverables to Sakai **and please upload to your git repository!!!!**

## Resources ##

Please review all materials below.

* Please look at links in 1.1 pdf under Week1
* [PC-Git Commands Cheat Sheet](https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html)
* [PC-Video Git 5 Minute Explanation](https://youtu.be/8oRjP8yj2Wo)
* [PC-Video Git Learn in 20 minutes](https://www.youtube.com/watch?v=Y9XZQO1n_7c)
* [PC-Create a Repository](https://confluence.atlassian.com/bitbucket/create-a-repository-800695642.html)
* [PC-Create a Team/User](https://confluence.atlassian.com/bitbucket/create-and-administer-your-team-665225537.html)
* [PC-Git vs Mercurial vs SVN](https://biz30.timedoctor.com/git-mecurial-and-cvs-comparison-of-svn-software/)
