## Assignment 3##

**Due Dates:**

- Homework please have it done by *10/05 11:55pm Friday*.


###Homework 3:###

Hand in under Sakai->Assignment->Homework_3


1. setup_class_user.sh:
   
   - Please follow the comments in the script in order to complete it.

   - The goal is to create users from a file (used the 2 col as username), add them to a group, and create a folder structure within a class folder under Documents

   - As well as output their signons and passwords to ur screen

   - Deliverable: A completed script AND copy and past the screen output in a file called users.txt


2. create a vagrantfile:

    - File should use a centos image 

    - Vagrantfile should have a provision to run the set_class_user.sh

    - Deliverable: Vagrantfile



Please upload the deliverables in a zip folder with ur <uid>-hw2.zip to Sakai **and please upload to your git repository as hw2!!!!**

## Resources ##

* Please look at links in 3.1 pdf under Week3 as well.
* More links available under Week3 it has the Bash and Vagrantfile examples there
