#!/bin/bash

DATE=`date '+%Y-%m-%d %H:%M:%S'`
status=$(curl --write-out "%{http_code}\n" --silent --output /dev/null localhost:5000)

echo -e "$DATE HTTP_STATUS: $status"
