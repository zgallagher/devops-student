Assignment 5
------------

**Due Dates:**

-	Homework please have it done by *10/10 11:59pm*.

### Homework 5:

Hand in under Sakai->Assignment->Homework_5 AND your repo under hw5

#### Premise

The homework was going to be a trade server and client, however the package dependency on quickfix in order to build it, took up to much memory. So I will leave that for another assignment. So instead we will be working with a flask app and checker.

#### General

1.	**SYSTEMD** You have a flask_app that will need to run as a daemon process with systemd.

	-	What is [flask](http://flask.pocoo.org/) it is a micro framework for Python. Basically a way to make a website. Look at the link. As that is the exact same program (hello_world.py) you will be daemonizing.

	-	Never worked with python? Well python (a dynamic non static language) is installed in CentOS, however its package manager which is called pip, is not.

		-	There is file called **yum_pkgs.txt** that needs to run first to install python's package manager. This will have to be install using the package manager of CentOS which is **yum**. This will be the **first step** before you do anything other steps.

	-	To check that the flask app is running, you can run the *lynx* command to see the webpage. lynx localhost:5000

2.	**CRON** You have a program called checker.sh, it will run every half hour of every hour and every day.

	-	What is does is, call the *wget* bash command and checks for a response from the URL that flask is hosting the website on, in this case localhost:5000

	-	Also redirect standard error and standard out to /var/log/checker.log for this application.

3.	Since these are two application you will have to create a **service user** for each program.

4.	You will store the programs in /opt/<program_name> and make sure that service user and group **OWN** the folders and everything *recursively* in there.

The end goal is to create a bash script that runs on provision in a vagrant file to do all the tasks necessary to get 1-4 done.

Remember that the CentOS image downloaded by vagrant is bare bones, so you have to install everything you need. Hence the file I created. A DevOps person would do a trial and error IRL check.

**Suggestion** Create the flask_app.service file first and the crontab entry as well. So it can

#### PreReq

Once the configs are written out create a folder called **app** put the config, flask_app, and checker folders in that folder. Then tar the folder **app**. Tar is compression like zip but linux based. Check it out at this [link](https://www.tecmint.com/18-tar-command-examples-in-linux/)

#### VagrantFile

-	Make sure to send the **app.tar.gz** to the /tmp folder of the image. [DOCUMENTATION](https://www.vagrantup.com/docs/provisioning/file.html)

-	Run yum-pkgs.txt to instal via provisioner [STACKOVERFLOW](https://unix.stackexchange.com/questions/47304/centos-install-packages-listed-in-a-text-file)[DOCUMENTATION](https://www.vagrantup.com/docs/provisioning/shell.html)

-	Then call the provisioning script, it doesn't have parameters so just call the script itself. [DOCUMENTATION](https://www.vagrantup.com/docs/provisioning/shell.html)

-	Make sure it is centos/7 the image, or 6 for some of you.

#### Provision.sh

-	In /opt -> create checker folder and flask_app folder

-	create service user flask_app and create user checker [DOCUMENTATION](https://superuser.com/questions/77617/how-can-i-create-a-non-login-user) Hint: use -r

-	in /tmp untar the app.tar.gz folder and cp code in checker to opt/checker and cp code from flask_app to /opt/flask_app

-	change ownership of /opt/checker to checker and do the same for flask_app **recursively is KEY**

-	the crontab you created for checker set it up to run

-	the flask_app.service file has to be enabled via systemctl

The last two things you can look at slides that were created, they have all the commands that you would need to run.

Once you are done, look in /var/log and look for checker.log and for flask_app do lynx localhost:5000 to see the webpage. Then you know you did it correctly.

**Deliverables:**

-	The app.tar.gz
-	yum_pkgs.txt
-	Vagrantfile
-	provision.sh

Please upload the deliverables in a zip folder with ur <uid>-hw5.zip to Sakai **and please upload to your git repository as hw5!!!**

Resources
---------

-	[Provisioners can be named!](https://www.vagrantup.com/docs/provisioning/basic_usage.html)
-	[CRONTAB QUICK EXAMPLES](http://www.thegeekstuff.com/2009/06/15-practical-crontab-examples)
